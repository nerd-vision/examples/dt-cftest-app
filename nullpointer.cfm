<cfscript>
    // page will receive an argument and perform a str length operation on that argument.
    // exception thrown if no argument passed in
    
    arg = URL["arg"];
   
    // triggers a null pointer
    WriteOutput("STRING LENGTH = ");
    WriteOutput( arg.length());
    WriteOutput("<BR><BR>");
    WriteOutput("STRING (ARG) = ");
    WriteOutput( arg ); 
</cfscript>

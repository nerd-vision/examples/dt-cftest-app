<cfset this.mappings["/cfc"] = getDirectoryFromPath(getCurrentTemplatePath()) & "cfc" />

<cfset this.name = "CFAPP">
<cfset this.sessionmanagement = true >
<cfset this.sessiontimeout ="#createTimespan(0,0,1,10)#" >

<cfset this.javaSettings = {LoadPaths = [".\exclude\javaLibs\"], loadColdFusionClassPath = true, reloadOnChange= true, watchInterval = 100, watchExtensions = "jar,class,xml"}>
<cfreturn true>
